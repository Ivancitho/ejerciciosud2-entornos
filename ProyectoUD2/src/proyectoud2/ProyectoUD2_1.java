package proyectoud2;

import java.util.Scanner;

public class ProyectoUD2_1 {

	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	
	System.out.println("Dame una edad");
	int leeredad = input.nextInt();
	
	boolean resultado = Metodo.esmenor(leeredad);
	
	if(resultado) {
		System.out.println("Es menor de edad");
	}
	else {
		System.out.println("No es menor");
	}
	
	System.out.println("Dame otra edad");
	int leeredad2 = input.nextInt();
	
	boolean resultado2 = Metodo.esmayor(leeredad2);
	
	if(resultado) {
		System.out.println("Es menor de edad");
	}
	else {
		System.out.println("No es menor");
	}
	
	System.out.println("Dame un caracter");
	char letra = input.nextLine().charAt(0);
	System.out.println(Metodo.digito(letra));
	
	input.close();

	}

}
